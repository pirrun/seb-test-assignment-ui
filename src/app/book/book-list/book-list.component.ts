import { Component, OnInit } from '@angular/core';
import {BookService} from "../../services/book.service";

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {
  books: Array<any>;
  sortOptions = ["10", "50", "100"];
  itemsPerPage: number;
  totalItems: any;
  page = 1;
  top=0;
  previousPage: any;

  constructor(private bookService: BookService) { }

  ngOnInit() {
    this.loadData();
  }

  calculateRating(books: Array<any>): void {
    this.books.forEach(book => {
      let rating = 0;
      book.ratings.forEach(rate => {
        rating += rate.rating;
      });

      if(book.ratings.length !== 0) {
        book.rating = rating/book.ratings.length;
      } else {
        book.rating = rating;
      }
    })
  }

  loadPage(page: number) {
    this.top = 0;
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.loadData();
    }
  }

  loadTopPage(top: number) {
    this.top = top;
    this.loadData();
  }

  loadData() {
    this.bookService.getAll({page: this.page - 1, top: this.top}).subscribe(data => {
      this.books = data['content'];
      this.totalItems = data['totalElements'];
      this.itemsPerPage = data['size'];
      this.calculateRating(this.books);
    });
  }
}
